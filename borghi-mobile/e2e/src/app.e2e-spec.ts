import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  describe('default screen', () => {
    beforeEach(() => {
      page.navigateTo('/HomePage');
    });
    it('should say HomePage', () => {
      expect(page.getParagraphText()).toContain('HomePage');
    });
  });
});
