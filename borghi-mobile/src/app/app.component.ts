import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Luoghi di interesse', url: '/luoghi-di-interesse', icon: 'paper-plane' },
    { title: 'Articoli', url: '/articoli', icon: 'book' },
    { title: 'Gallery', url: '/gallery', icon: 'images' },
    { title: 'Suggerimenti', url: '/suggerimenti', icon: 'text' },
  ];
  
  constructor() {}
}
