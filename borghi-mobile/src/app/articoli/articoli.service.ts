import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Articolo, Categoria } from './articolo.model';
@Injectable({
providedIn: 'root'
})
export class ArticoliService {
articoli: Articolo[] = [];
categorie: Categoria[] = [];

constructor(private http: HttpClient) {
  // this.loadFakeArticoli();
}

getArticoli(): Promise<Articolo[]> {
    // return Promise.resolve(this.articoli);
    const url = 'http://localhost/wp_primo_project_work/wp-json/wp/v2/posts';
    return this.http.get<Articolo[]>(url).toPromise();
    }
    
getArticolo(id): Promise<Articolo> {
    //   let result: Articolo = null;
    
    //   for (let i = 0; i < this.articoli.length; i++){
    //     if(this.articoli[i].id == id ){
    //       result = this.articoli[i];
    //       break;
    //     }
    //   }
    // return Promise.resolve(result);
    const url = 'http://localhost/wp_primo_project_work/wp-json/wp/v2/posts' + id;
    return this.http.get<Articolo>(url).toPromise();
    }

getCategorie(): Promise<Categoria[]> {
    // return Promise.resolve(this.categorie);
    const url = 'http://localhost/wp_primo_project_work/wp-json/wp/v2/categories';
    console.log(url);
    return this.http.get<Categoria[]>(url).toPromise();
    }
    
getCategoria(id): Promise<Categoria> {
    //   let result: Categoria = null;
    
    //   for (let i = 0; i < this.categorie.length; i++){
    //     if(this.categorie[i].id == id ){
    //       result = this.categorie[i];
    //       break;
    //     }
    //   }
    // return Promise.resolve(result);
    const url = 'http://localhost/wp_primo_project_work/wp-json/wp/v2/categories' + id;
    return this.http.get<Categoria>(url).toPromise();
    }


}