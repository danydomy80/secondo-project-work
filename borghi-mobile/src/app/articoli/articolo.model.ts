export class Articolo {
    id: number;
    name: string;
    year: number;
    status: string;
    title: {};
    content: {};
    excerpt: {};
    _links: {}
    }

export class Categoria {
    id: number;
    name: string;
    count: number;
    }