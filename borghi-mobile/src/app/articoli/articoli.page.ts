import { Component, OnInit } from '@angular/core';
import { ArticoliService } from './articoli.service';
import { Articolo, Categoria } from './articolo.model';

@Component({
  selector: 'app-articoli',
  templateUrl: './articoli.page.html',
  styleUrls: ['./articoli.page.scss'],
})
export class ArticoliPage implements OnInit {
  articoli: Articolo[];
  categorie: Categoria[];

  constructor(
    private articoliService: ArticoliService) { }

  ngOnInit() {
    this.articoliService.getArticoli()
      .then(list => {
        this.articoli = list;
      });
    this.articoliService.getCategorie()
      .then(list => {
        this.categorie = list;
      });
  }

}
