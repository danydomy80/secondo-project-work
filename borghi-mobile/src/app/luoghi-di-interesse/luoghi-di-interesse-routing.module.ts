import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LuoghiDiInteressePage } from './luoghi-di-interesse.page';

const routes: Routes = [
  {
    path: '',
    component: LuoghiDiInteressePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LuoghiDiInteressePageRoutingModule {}
