import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LuoghiDiInteressePage } from './luoghi-di-interesse.page';

describe('LuoghiDiInteressePage', () => {
  let component: LuoghiDiInteressePage;
  let fixture: ComponentFixture<LuoghiDiInteressePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LuoghiDiInteressePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LuoghiDiInteressePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
