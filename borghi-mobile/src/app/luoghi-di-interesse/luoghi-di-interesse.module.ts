import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LuoghiDiInteressePageRoutingModule } from './luoghi-di-interesse-routing.module';

import { LuoghiDiInteressePage } from './luoghi-di-interesse.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LuoghiDiInteressePageRoutingModule
  ],
  declarations: [LuoghiDiInteressePage]
})
export class LuoghiDiInteressePageModule {}
