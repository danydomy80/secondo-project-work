import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuggerimentiPage } from './suggerimenti.page';

const routes: Routes = [
  {
    path: '',
    component: SuggerimentiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuggerimentiPageRoutingModule {}
