import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuggerimentiPageRoutingModule } from './suggerimenti-routing.module';

import { SuggerimentiPage } from './suggerimenti.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuggerimentiPageRoutingModule
  ],
  declarations: [SuggerimentiPage]
})
export class SuggerimentiPageModule {}
