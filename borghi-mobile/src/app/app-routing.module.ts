import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  // {
    //   path: 'folder/:id',
    //   loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
    // },
    {
      path: 'suggerimenti',
      loadChildren: () => import('./suggerimenti/suggerimenti.module').then( m => m.SuggerimentiPageModule)
    },
    {
      path: 'gallery',
      loadChildren: () => import('./gallery/gallery.module').then( m => m.GalleryPageModule)
    },
    {
      path: 'articoli',
      loadChildren: () => import('./articoli/articoli.module').then( m => m.ArticoliPageModule)
    },
    {
      path: 'luoghi-di-interesse',
      loadChildren: () => import('./luoghi-di-interesse/luoghi-di-interesse.module').then( m => m.LuoghiDiInteressePageModule)
    },
    {
      path: 'home',
      loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
    },
    {
      path: '**',
      redirectTo: 'home'
    },
  ];
  
  @NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
